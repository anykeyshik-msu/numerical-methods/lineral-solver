#include "systems.h"
#include "gauss.h"

#include <stdio.h>

#include "SF1.c"

int main(void)
{
    size_t N;
    puts("Please, input the size of Matrix:");
    scanf("%zu", &N);
    puts("Please, input the parameter M:");
    scanf("%lf", &paramM);
    puts("Please, input the parameter N:");
    scanf("%lf", &paramN);

    system_t *S = make_func_system(N, genMatrix, genVector);

    clear_stats();
    print_header("Generated System");
    print_system(S, 4, 8);
    if (solve_gauss(S, max_elem)) {
        print_solution(S, 8);
    }
    else {
        print_system(S, 4, 8);
    }
    print_stats();

    del_system(S);
}
