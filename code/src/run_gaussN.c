#include "systems.h"
#include "gauss.h"

void example(system_t *s, char *name);

int main(void)
{
    system_t *S1 = make_norm_system(

#include "SN1_1.c"

    );
    system_t *S2 = make_norm_system(

#include "SN1_2.c"

    );
    system_t *S3 = make_norm_system(

#include "SN1_3.c"

    );
    system_t *S4 = make_norm_system(

#include "SN1_4.c"

    );

    example(S1, "1");
    example(S2, "2");
    example(S3, "3");
    example(S4, "4");

    del_system(S1);
    del_system(S2);
    del_system(S3);
    del_system(S4);
}

void example(system_t *S, char *name)
{
    clear_stats();
    print_header(name);
    print_system(S, 4, 8);
    if (solve_gauss(S, max_elem)) {
        print_solution(S, 8);
    }
    else {
        print_system(S, 4, 8);
    }
    print_stats();
}
