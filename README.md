# Linear Solver

Task for Numerical Methods course on CMC MSU

### Build
Run in project dir:
```
$ cd code
$ mkdir build && cd build
$ cmake .. && make
```
Binary files will be appear in `$PROJECT_DIR/code/bin` directory
