#include <string.h>
#include <stdio.h>

#include "systems.h"

stats_t st = {0 };

void print_header(const char *name)
{
    int name_len = strlen(name);
    int screen_size = (SCREEN_WIDTH - name_len) / 2;
    int odd = (name_len + 2 * screen_size == SCREEN_WIDTH);
    printf("%.*s %.*s %.*s\n",
           screen_size - 1, HEADER,
           name_len, name,
           screen_size - odd, HEADER);
}

void clear_stats(void)
{
    const stats_t empty = {0 };
    st = empty;
}

void print_stats(void)
{
    print_header("Current Stats");
    printf("%18s: %20lf %s\n", "Time", 1.0 * st.clock / CLOCKS_PER_SEC, "seconds");
    printf("%18s: %20ju %s\n", "Operations", st.add_count, "addition");
    printf("%18s: %20ju %s\n", "Operations", st.mul_count, "multiplication");
    printf("%18s: %20ju %s\n", "Memory", st.get_count, "reads");
    printf("%18s: %20ju %s\n", "Memory", st.set_count, "writes");
    printf("%18s: %20ju\n", "Total Iterations", st.itr_count);
    fputs("\n", stdout);
}

void print_equation(equation_t *equation, size_t skip, int precision)
{
    size_t lenStart;
    size_t lenEnd = 0;

    if (skip && equation->len > 2*skip) {
        lenStart = lenEnd = skip;
    }
    else {
        lenStart = equation->len;
    }

    for (size_t i = 0; i < lenStart; i++) {
        printf("%*.*lf ", precision + 4, precision, equation->lhs[i]);
    }
    if (lenEnd) {
        fputs("... ", stdout);
    }

    for (size_t i = 1; i <= lenEnd; i++) {
        printf("%*.*lf ", precision + 4, precision, equation->lhs[equation->len-i]);
    }

    printf("= %*.*lf\n", precision + 4, precision, equation->rhs);
}

void print_system(system_t *system, size_t skip, int precision)
{
    print_header("System of Linear Equations");
    size_t lenStart;
    size_t lenEnd = 0;
    if (skip && system->len > 2 * skip) {
        lenStart = lenEnd = skip;
    }
    else {
        lenStart = system->len;
    }

    for (size_t i = 0; i < lenStart; i++) {
        print_equation(system->sys[i], skip, precision);
    }
    if (lenEnd) {
        puts("\t\t...\t\t\t\t\t...\t\t\t\t\t...");
    }

    for (size_t i = 1; i <= lenEnd; i++) {
        print_equation(system->sys[system->len - i], skip, precision);
    }
    fputs("\n", stdout);
}

void print_solution(system_t *system, int precision)
{
    print_header("Solution");
    size_t lenSys = system->len;

    for (size_t i = 0; i < lenSys; i++) {
        printf("x%zu = %*.*lf\n", i, precision + 4, precision, system->sys[i]->rhs);
    }
    fputs("\n", stdout);
}

system_t * alloc_system(size_t size)
{
    system_t *S = malloc((sizeof(*S) + sizeof(*(S->sys))) * size);

    for (size_t i = 0; i < size; i++) {
        S->sys[i] = malloc(sizeof(*(S->sys[i])) + sizeof(*(S->sys[i]->lhs)) * size);
        S->sys[i]->len = size;
    }
    S->len = size;

    return S;
}

void del_system(system_t *system)
{
    size_t N = system->len;

    for (size_t i = 0; i < N; i++) {
        free(system->sys[i]);
    }
    free(system);
}


system_t * make_func_system(size_t size, matrix_gen_t matrix_gen, vector_get_t vector_gen)
{
    system_t *S = alloc_system(size);

    for (size_t i = 0; i < size; i++) {
        S->sys[i]->rhs = vector_gen(i);

        for (size_t j = 0; j < size; j++) {
            S->sys[i]->lhs[j] = matrix_gen(i, j);
        }
    }

    return S;
}

system_t * make_norm_system(size_t N, const double *matrix, const double *vector)
{
    system_t *S = alloc_system(N);

    for (size_t i = 0; i < N; i++)
    {
        S->sys[i]->rhs = vector[i];

        for (size_t j = 0; j < N; j++) {
            S->sys[i]->lhs[j] = matrix[i*N + j];
        }
    }

    return S;
}
