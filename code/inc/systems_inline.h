inline double get_eq_elem(system_t *system, size_t i, size_t j)
{
    st.get_count++;
    return system->sys[i]->lhs[j];
}

inline void set_eq_elem(system_t *system, double f, size_t i, size_t j)
{
    st.set_count++;
    system->sys[i]->lhs[j] = f;
}

inline double get_res_elem(system_t *S, size_t i)
{
    st.get_count++;
    return S->sys[i]->rhs;
}

inline void set_res_elem(system_t *S, double f, size_t i)
{
    st.set_count++;
    S->sys[i]->rhs = f;
}

inline void swap_equations(system_t *system, size_t i, size_t j)
{
    equation_t *tmp = system->sys[i];
    system->sys[i] = system->sys[j];
    system->sys[j] = tmp;
}

inline bool normalize(system_t *system, size_t size)
{
    double elem = get_eq_elem(system, size, size);
    double temp[system->len];

    size_t N = system->len;

    for (size_t j = size; j < N; j++) {
        double tmp = get_eq_elem(system, size, j);
        temp[j] = div_with_count(tmp, elem);

        if (!isfinite(temp[j])) {
            return false;
        }
    }

    double tmp = get_res_elem(system, size);
    tmp = div_with_count(tmp, elem);
    if (!isfinite(tmp)) {
        return false;
    }

    set_res_elem(system, tmp, size);
    for (size_t j = size; j < N; j++) {
        set_eq_elem(system, temp[j], size, j);
    }

    return true;
}

inline double add_with_count(double a, double b)
{
    st.add_count++;
    return a + b;
}

inline double mul_with_count(double a, double b)
{
    st.mul_count++;
    return a * b;
}

inline double div_with_count(double a, double b)
{
    // For now, count division as multiplication.
    st.mul_count++;
    return a / b;
}
