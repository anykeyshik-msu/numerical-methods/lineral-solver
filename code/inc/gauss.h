#ifndef _GAUSS_H
#define _GAUSS_H
// ------------------------------- Include section ----------------------------

#include "systems.h"

// ------------------------------- Type section -------------------------------

typedef size_t selector(system_t *, size_t) __attribute__((nonnull));

// ------------------------------- Declaration section ------------------------

bool solve_gauss(system_t *system, selector aSelector) __attribute__((nonnull));
selector max_elem;

#endif
