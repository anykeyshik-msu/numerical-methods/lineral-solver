#ifndef _RELAX_H
#define _RELAX_H
// ------------------------------- Include section ----------------------------

#include "systems.h"

// ------------------------------- Declaration section ------------------------

bool solve_relax(system_t *sys, double omega, double precision) __attribute__((nonnull));
bool iter_relax(system_t *sys, double omega, double *step) __attribute__((nonnull));
double vec_diff(size_t size, double *vec1, double *vec2) __attribute__((nonnull));
void vec_init(size_t size, double *vec) __attribute__((nonnull));
void vec_cpy(size_t size, double *dst, const double *src) __attribute__((nonnull));

#endif
