#include <stdio.h>

#include "systems.h"
#include "relax.h"

void example(system_t *S, char *name, double omega, double precision);

int main(void)
{
    system_t *S1 = make_norm_system(

#include "SN2_1.c"

    );
    system_t *S2 = make_norm_system(

#include "SN2_2.c"

    );
    system_t *S3 = make_norm_system(

#include "SN2_3.c"

    );
    system_t *S4 = make_norm_system(

#include "SN2_4.c"

    );

    double omega, precision;
    puts("Please input the iteration parameter:");
    scanf("%lf", &omega);
    puts("Please input the desired precision:");
    scanf("%lf", &precision);

    example(S1, "1", omega, precision);
    example(S2, "2", omega, precision);
    example(S3, "3", omega, precision);
    example(S4, "4", omega, precision);

    del_system(S1);
    del_system(S2);
    del_system(S3);
    del_system(S4);
}

void example(system_t *S, char *name, double omega, double precision)
{
    clear_stats();
    print_header(name);
    size_t digits = log10(1/precision);
    print_system(S, 4, digits);
    if (solve_relax(S, omega, precision)) {
        print_solution(S, digits);
    }
    print_stats();
}
