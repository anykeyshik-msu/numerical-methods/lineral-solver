#ifndef _SYSTEMS_H
#define _SYSTEMS_H
// ------------------------------- Include section ----------------------------

#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <math.h>

// ------------------------------- Define section -----------------------------

#ifndef HEADER_SEP
#define HEADER_SEP "="
#endif

#ifndef SCREEN_WIDTH
#define SCREEN_WIDTH 80
#endif

#define REP(c) c c c c c c c c c c c c c c c c
#define HEADER REP(REP(HEADER_SEP))

// ------------------------------- Type section -------------------------------

// A single linear equation of form
// lhs[0]x(0) + lhs[1]x(1) + ... + lhs[len-1]x(len-1) = rhs.
typedef struct
{
    size_t len;
    double rhs;
    double lhs[];
} equation_t;

// A system of linear equations Ax = b.
typedef struct
{
    size_t len;
    equation_t *sys[];
} system_t;

// Global state segment for timing and performance comparison.
typedef struct
{
    clock_t clock;
    size_t itr_count;
    size_t mul_count;
    size_t add_count;
    size_t get_count;
    size_t set_count;
} stats_t;
extern stats_t st;

// Types for functional definition of systems of linear equations.
typedef double matrix_gen_t(size_t, size_t);
typedef double vector_get_t(size_t);

// ------------------------------- Declaration section ------------------------

void print_equation(equation_t *equation, size_t skip, int precision) __attribute__((nonnull));
void print_system(system_t *system, size_t skip, int precision) __attribute__((nonnull));
void print_solution(system_t *system, int precision) __attribute__((nonnull));
void print_header(const char *name) __attribute__((nonnull));
void clear_stats(void);
void print_stats(void);
void del_system(system_t *system) __attribute__((nonnull));
system_t * alloc_system(size_t size);
system_t * make_func_system(size_t size, matrix_gen_t matrix_gen, vector_get_t vector_gen) __attribute__((nonnull));
system_t * make_norm_system(size_t size, const double *matrix, const double *vector) __attribute__((nonnull));

// ------------------------------- Inline section -----------------------------

inline double get_eq_elem(system_t *system, size_t i, size_t j)
    __attribute__((always_inline))
    __attribute__((nonnull));

inline void set_eq_elem(system_t *system, double f, size_t i, size_t j)
    __attribute__((always_inline))
    __attribute__((nonnull));

inline double get_res_elem(system_t *S, size_t i)
    __attribute__((always_inline))
    __attribute__((nonnull));

inline void set_res_elem(system_t *S, double f, size_t i)
    __attribute__((always_inline))
    __attribute__((nonnull));

inline void swap_equations(system_t *system, size_t i, size_t j)
    __attribute__((always_inline))
    __attribute__((nonnull));

inline bool normalize(system_t *system, size_t size)
    __attribute__((always_inline))
    __attribute__((nonnull));

inline double mul_with_count(double, double)
    __attribute__((always_inline));

inline double div_with_count(double, double)
    __attribute__((always_inline));

inline double add_with_count(double, double)
    __attribute__((always_inline));

#include "systems_inline.h"

#endif
