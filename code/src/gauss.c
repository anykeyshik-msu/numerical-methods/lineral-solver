#include <stdio.h>

#include "gauss.h"

size_t max_elem(system_t *system, size_t size)
{
    double tmp;
    size_t N = system->len;
    double max = get_eq_elem(system, size, size);
    size_t ret = size;

    for (size_t i = size + 1; i < N; i++) {
        if ((tmp = fabs(get_eq_elem(system, i, size))) > max) {
            max = tmp;
            ret = i;
        }
    }

    return ret;
}

bool solve_gauss(system_t *system, selector aSelector)
{
    clock_t start = clock();
    size_t N = system->len;

    for (size_t i = 0; i < N; i++)
    {
        st.itr_count++;
        size_t swap = aSelector(system, i);

        if (swap != i) {
            swap_equations(system, i, swap);
        }
        if(!normalize(system, i)) {
            print_header("Error");
            printf("Couldn't find solution, possibly a singular system.\n");
            st.clock += clock() - start;

            return false;
        }

        for (size_t j = i+1; j < N; j++) {
            double coeff = get_eq_elem(system, j, i);

            for (size_t k = i; k < N; k++) {
                double tmp = get_eq_elem(system, i, k);
                tmp = mul_with_count(tmp, coeff);
                tmp = add_with_count(get_eq_elem(system, j, k), -tmp);
                set_eq_elem(system, tmp, j, k);
            }

            double tmp = get_res_elem(system, i);
            tmp = mul_with_count(tmp, coeff);
            tmp = add_with_count(get_res_elem(system, j), -tmp);
            set_res_elem(system, tmp, j);
        }
    }
    for (size_t i = 1; i < N; i++) {
        st.itr_count++;
        double elem = get_res_elem(system, N - i);

        for (size_t j = i+1; j <= N; j++) {
            double coeff = get_eq_elem(system, N - j, N - i);
            set_eq_elem(system, 0.0, N - j, N - i);

            double tmp = mul_with_count(elem, coeff);
            tmp = add_with_count(get_res_elem(system, N - j), -tmp);
            set_res_elem(system, tmp, N - j);
        }
    }
    st.clock += clock() - start;

    return true;
}
