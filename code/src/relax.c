#include <math.h>

#include "relax.h"

#include <stdio.h>

bool iter_relax(system_t *sys, double omega, double *step)
{
    size_t N = sys->len;
    for (size_t i = 0; i < N; i++) {
        double sigma = 0;

        for (size_t j = 0; j < N; j++) {
            if (i != j) {
                double tmp = mul_with_count(get_eq_elem(sys, i, j), step[j]);
                sigma = add_with_count(sigma, tmp);
            }
        }

        double tmp = add_with_count(get_res_elem(sys, i), -sigma);
        tmp = add_with_count(div_with_count(tmp, get_eq_elem(sys, i, i)), -step[i]);
        step[i] = add_with_count(step[i], mul_with_count(omega, tmp));

        if (!isfinite(step[i])) {
            return false;
        }
    }

    return true;
}

double vec_diff(size_t size, double *vec1, double *vec2)
{
    double accum = 0.0;

    for (size_t i = 0; i < size; i++) {
        double tmp = add_with_count(vec1[i], -vec2[i]);
        accum += mul_with_count(tmp, tmp);
    }

    return sqrt(accum);
}

void vec_init(size_t size, double *vec)
{
    for (size_t i = 0; i < size; i++) {
        vec[i] = 0;
    }
}

void vec_cpy(size_t size, double *dst, const double *src)
{
    for (size_t i = 0; i < size; i++) {
        dst[i] = src[i];
    }
}

bool solve_relax(system_t *sys, double omega, double precision)
{
    clock_t start = clock();
    size_t size = sys->len;
    double step[size];
    double last[size];
    double step_len = 0.0;
    size_t counter = 0;
    vec_init(size, step);

    do {
        counter++;
        st.itr_count++;
        vec_cpy(size, last, step);

        if (!iter_relax(sys, omega, step) || !isfinite(step_len))
        {
            print_header("Error");
            puts("Method doesn't converge.");
            st.clock += clock() - start;

            return false;
        }
        if (!(counter % 500)) {
            printf("Iteration #%zu, ||xi - xi+1|| = %lf. "
                   "If this takes too long, the method may be diverging.\n",
                   counter, step_len);
        }
    } while ((step_len = vec_diff(size, last, step)) > precision/2);
    
    for (size_t i = 0; i < size; i++) {
        set_res_elem(sys, step[i], i);
    }

    st.clock += clock() - start;

    return true;
}
