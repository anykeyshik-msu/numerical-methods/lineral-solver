#include "systems.h"
#include "relax.h"

#include <stdio.h>

#include "SF2.c"

int main(void)
{
    size_t N;
    double omega;
    double precision;
    puts("Please, input the size of Matrix:");
    scanf("%zu", &N);
    puts("Please, input the parameter M:");
    scanf("%lf", &paramM);
    puts("Please, input the parameter N:");
    scanf("%lf", &paramN);
    puts("Please, input the iteration parameter:");
    scanf("%lf", &omega);
    puts("Please, input the desired precision:");
    scanf("%lf", &precision);

    system_t *S = make_func_system(N, genMatrix, genVector);

    clear_stats();
    print_header("Generated System");
    size_t digits = log10(precision);
    print_system(S, 4, digits);
    if (solve_relax(S, omega, precision)) {
        print_solution(S, digits);
    }
    print_stats();

    del_system(S);
}
